# Weather App
Basic weather app to explore the Vue3 Composition API. Just an example for now.

See it in action [here](https://d2b7n910mx3z71.cloudfront.net/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

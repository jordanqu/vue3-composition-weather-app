import { onMounted, reactive, computed } from "vue";

export const appState = () => {

  // Store all computed functions
  const computedFuncs = {
    description: computed(() => data.weather.current.weather[0].description),
    min: computed(() => data.weather.daily[0].temp.min.toFixed(0)),
    max: computed(() => data.weather.daily[0].temp.max.toFixed(0)),
    current: computed(() => data.weather.current.temp.toFixed(0)),
    hourly: computed(() => data.weather.hourly.slice(0, 24)),
    daily: computed(() => data.weather.daily.slice(1)),
    humidity: computed(() => data.weather.current.humidity),
    precipitation: computed(() => data.weather.daily[0].rain.toFixed(0)),
    icon: computed(() => data.weather.current.weather[0].icon)
  }

  // Set our reactive data. Seems we need to be specific here
  const data = reactive({
    ...computedFuncs,
    api_key: 'cbfc28e1909d8facebe7566910bc14ca',
    location: {
      latitude: '',
      longitude: '',
      name: ''  
    },
    weather: {
      current: {
        weather: [
          {
            description: ''
          }
        ],
        temp: 0,
        humidity: 0,
      },
      daily: [
        {
          weather: [
            {
              icon: ''
            }
          ],
          temp: {
            min: 0,
            max: 0
          },
          rain: 0
        }
      ],
      hourly: []
    },
    loading: false,
    hourTemps: []
  })

  // Get user coords using navigator API
  const getCoords = async () => {
    const { coords } = await new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    })
    data.location.latitude = coords.latitude;
    data.location.longitude = coords.longitude;
    getWeather();
  }

  // Return a backgorund image for the icon code
  // Icons references are here https://openweathermap.org/weather-conditions
  const backgroundImg = () => {
    const imgs = {
      "01d": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/sunny.jpg",
      "01n": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/sunny.jpg",
      "02n": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/sunny.jpg",
      "04n": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/cloudy.jpg",
      "04d": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/cloudy.jpg",
      "09d": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/rain.jpg",
      "09n": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/rain.jpg",
      "10d": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/rain.jpg",
      "10n": "https://examplebucketvue3weather.s3-ap-southeast-2.amazonaws.com/rain.jpg",
    }
    return imgs[data.icon]
  }

  // Get our weather forecast from API
  const getWeather = () => {
    fetch(`https://api.openweathermap.org/data/2.5/onecall?appid=${data.api_key}&lat=${data.location.latitude}&lon=${data.location.longitude}&exclude=minutely&units=metric`)
    .then(res => res.json())
    .then(json => {
      console.log("getWeather()", json);
      data.weather = json;
      data.hourTemps = hourlyTemps(json.hourly.slice(0,24))
      data.loading = false;
      getLocation();
    })
  }

  // This will return user-friendly string of the user location
  const getLocation = () => {
    fetch(`http://api.openweathermap.org/geo/1.0/reverse?appid=${data.api_key}&lat=${data.location.latitude}&lon=${data.location.longitude}&limit=2`)
    .then(res => res.json())
    .then(json => {
      console.log("getLocation()", json);
      data.location.name = json[0].name;
    })
  }

  // Format the date to hours in human friendly format
  const formatHour = (dt) => {
    let date = new Date(dt*1000);
    let hour = date.getHours();
    let suffix = hour >= 12 ? "PM":"AM"; 
    return ((hour + 11) % 12 + 1) + suffix;
  }

  // Format the datetime as human readable name
  const getDay = (dt) => {
    return new Date(dt*1000).toLocaleDateString('en-US', { weekday: 'long' });
  }

  // Need hourly temps in our graph later
  const hourlyTemps = (hours) => {
    let arr = [];
    hours.forEach(hour => {
      arr.push(parseInt(hour.temp.toFixed(0)));
    })
    return arr
  }

  onMounted(async () => {
    await getCoords();
  })

  return {
    data,
    backgroundImg,
    formatHour,
    getDay
  }

}